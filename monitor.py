#!/usr/bin/python3

import configparser
from time import strftime, sleep
from sys import argv
import os, platform
import re
import shutil
import mailalarm

# class Monitor
class Monitor:

    # Monitor Constructor
    def __init__(self):
        # Set the instance variables
        self.logfile = "./logging.log"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~ ARGUMENT Section ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        # Givven Arguments we're processed, each will exit() the script after an output
        if "--getHostname" in argv:
            print(str(self.getHostname()))
            exit()

        if "--getVersion" in argv:
            print(str(self.getVersion()))
            exit()

        if "--getCPUname" in argv:
            print(str(self.getCPUname()))
            exit()

        if "--getRAMamount" in argv:
            print(str(self.getRAMamount()))
            exit()

        if "--getCPUusage" in argv:
            print(str(self.getCPUusage()))
            exit()

        if "--getRAMusage" in argv:
            print(str(self.getRAMusage()))
            exit()

        if "--getStorage" in argv:
            print(str(self.getStorage()))
            exit()

        if "--getStorageusage" in argv:
            print(str(self.getStorageusage()))
            exit()

        # Write into logfile
        if "-l" in argv:
            self.log()
            exit()

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~ Main Functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        # Mainloop
        while True:
            # It will be written into the logfile; Alarm procedure is started
            self.log()
            self.alarm()
            exit()

    # Function alarm;
    def alarm(self):
        # If CPU usage is above critical, create a new thread which creates a mail notification
        if self.checkCPUusage() == 2:
            self.createMail("Cpu")

        # If RAM usage is above critical, create a new thread which creates a mail notification
        if self.checkRAMusage() == 2:
            self.createMail("Ram")

    # Function createMail; needed parameters: string type;
    def createMail(self, type):
        # Get the mail subject and content out of settings.ini
        subject = self.getSettings("MAIL", "mailSubject")
        text = self.getSettings("MAIL", "mailText" + type) + "\nTime: " + self.getCurrentTimestamp()

        # Create an object of MailAlarm and start the function send_mail() with subject and text
        mail = mailalarm.MailAlarm()
        mail.send_mail(subject, text)

    # Function getSettings; needed parameters: string index, string key
    @staticmethod
    def getSettings(index, key):
        # Read the settings.ini file with configparser and return a value
        settings = configparser.ConfigParser()
        settings.read("./settings.ini")
        return settings[index][key]

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~ CHECK Usage Functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # Function checkCPUusage;
    def checkCPUusage(self):
        # Get the CPU usage
        cpu_usage = self.getCPUusage()
        # Get the CPU hard and soft limits out of settings.ini file
        cpu_soft = self.getSettings("CPULIMIT", "softlimitCPU")
        cpu_hard = self.getSettings("CPULIMIT", "hardlimitCPU")
        # Return integer 0,1 or 2; 0=ok; 1=between soft and hard limit; 2=above hard limit;
        if int(cpu_soft) < cpu_usage < int(cpu_hard):
            return int(1)
        elif cpu_usage > int(cpu_hard):
            return int(2)
        else:
            return int(0)

    # Function checkRAMusage;
    def checkRAMusage(self):
        # Get the RAM usage
        ram_usage = self.getRAMusage()
        # Get the RAM hard and soft limits out of settings.ini file
        ram_soft = self.getSettings("RAMLIMIT", "softlimitRAM")
        ram_hard = self.getSettings("RAMLIMIT", "hardlimitRAM")
        # Return integer 0,1 or 2; 0=ok; 1=between soft and hard limit; 2=above hard limit;
        if int(ram_soft) < ram_usage < int(ram_hard):
            return int(1)
        elif ram_usage > int(ram_hard):
            return int(2)
        else:
            return int(0)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~ GET Information Functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # Function getHostname;
    def getHostname(self):
        # Return the hostname of the running machine, using os library
        return os.getenv('HOSTNAME', os.getenv('COMPUTERNAME', platform.node())).split('.')[0]

    # Function getVersion;
    def getVersion(self):
        # Read the UNIX version out of version file; return as string
        version = open("/proc/version", "r")
        signature = version.readline()
        version.close()
        return str(signature)

    # Function getCPUname;
    def getCPUname(self):
        # Read the CPU model name out of cpuinfo file
        version = open("/proc/cpuinfo", "r")
        lines = list()
        version.seek(0)
        try:
            for line in version:
                lines.append(line.strip())
        finally:
            version.close()
        # Edit the string with regular expressions and return as string
        result = re.sub("model name", "", str(lines[4]))
        result = re.sub(": ", "", result)
        result = re.sub(r"\s+", "", result)
        return result

    # Function getRAMamount;
    def getRAMamount(self):
        # Read the installed RAM out of meminfo;
        # return value in GB with 1 decimal as string
        meminfo = open("/proc/meminfo", "r")
        total = meminfo.readline()
        meminfo.close()
        total = total.split()[1]
        return str(round(float(total)/1000000,1))

    # Function getStorage;
    def getStorage(self):
        # Return the total available disk space using shutil library;
        # return value in GB with 1 decimal as string
        total, used, free = shutil.disk_usage(__file__)
        return str(round(float(total)/1024/1024/1024,1))

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~ GET Usage Functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # Function getCPUusage;
    def getCPUusage(self):
        # Read the load average out of loadavg file and return the first 4 digits;
        # return value in percentage as integer
        loadavg = open("/proc/loadavg", "r")
        usage = loadavg.read(4)
        loadavg.close()
        usage = usage[2:]
        return int(usage)

    # Function getRAMusage;
    def getRAMusage(self):
        # Read the memory info out of meminfo file and return the used RAM;
        # return value in percentage as integer
        meminfo = open("/proc/meminfo", "r")
        total = meminfo.readline()
        free = meminfo.readline()
        meminfo.close()
        total = total.split()[1]
        free = free.split()[1]
        used = 100 - (float(free) * 100 / float(total))
        return int(used)

    # Function getStorageusage;
    def getStorageusage(self):
        # Return the used disk space using shutil library;
        # return value in percentage as integer
        total, used, free = shutil.disk_usage(__file__)
        storage = (used/total)*100
        return int(storage)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~ LOG Functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # Function getCurrentTimestamp;
    def getCurrentTimestamp(self):
        # Return the current timestamp as string in format: 01.01.1970 00:00:00
        return strftime("%d.%m.%Y %H:%M:%S")

    # Function appendTimestamp; needed parameters: string value, string name
    def appendTimestamp(self, value, name):
        # Return a string in logfile format
        return self.getCurrentTimestamp() + " --- " + name + ": " + str(value) + "%"

    # Function log;
    def log(self):
        # Open the logfile, append log text at the end of the file and close the file
        file = open(self.logfile, "a")
        file.write(self.appendTimestamp(self.getRAMusage(), "RAM") + "\n")
        file.write(self.appendTimestamp(self.getCPUusage(), "CPU") + "\n")
        file.write(self.appendTimestamp(self.getStorageusage(), "Disk") + "\n")
        file.close()

# Create an instance of the class Monitor
Monitor = Monitor()