<html>
    <head>
        <title>Server Monitoring</title>
        <link rel="icon" type="image/vnd.microsoft.icon" href="favicon.ico">
        <link rel="stylesheet" type="text/css" href="style.css" />
        <script language="javascript" type="text/javascript" src="script.js"></script>
    </head>
    <body onload="bar_init()">

<?php
$grepdate = date("d.m");
function arrayToString($out){
    return implode("<br>", $out);
}

if(!isset($_GET["param"])){
    exec("/var/www/monitor.paulfruehauf.de/monitor.py --getHostname", $hostname);
    exec("/var/www/monitor.paulfruehauf.de/monitor.py --getVersion", $version);
    exec("/var/www/monitor.paulfruehauf.de/monitor.py --getCPUname", $cpuname);
    exec("/var/www/monitor.paulfruehauf.de/monitor.py --getRAMamount", $ramamount);
    exec("/var/www/monitor.paulfruehauf.de/monitor.py --getStorage", $storage);
    exec("/var/www/monitor.paulfruehauf.de/monitor.py --getCPUusage", $cpuusage);
    exec("/var/www/monitor.paulfruehauf.de/monitor.py --getRAMusage", $ramusage);
    exec("/var/www/monitor.paulfruehauf.de/monitor.py --getStorageusage", $diskusage);
    echo "<div id=\"wrapper\">";
    echo "<div class=\"attribute\"><attrtitle>Hostname: </attrtitle><value>".arrayToString($hostname)."</value></div>";
    echo "<div class=\"attribute\"><attrtitle>Linux OS: </attrtitle><value>".arrayToString($version)."</value></div>";
    echo "<div class=\"attribute\"><attrtitle>CPU Model: </attrtitle><value>".arrayToString($cpuname)."</value></div>";
    echo "<div class=\"attribute\"><attrtitle>Installed RAM: </attrtitle><value>".arrayToString($ramamount)."GB</f></div>";
    echo "<div class=\"attribute\" style=\"margin-bottom: 50px;\"><attrtitle>Installed Disk Space: </attrtitle><value>".arrayToString($storage)."GB</value></div>";

    echo "<div class=\"hidden\" id=\"cpu\">".arrayToString($cpuusage)."</div>";
    echo "<div class=\"hidden\" id=\"ram\">".arrayToString($ramusage)."</div>";
    echo "<div class=\"hidden\" id=\"disk\">".arrayToString($diskusage)."</div>";

    echo "<div id=\"bar_wrapper\">";
    echo "<div class=\"label\">CPU Usage:</div><div class=\"bar\"><div class=\"percent\" id=\"percent_cpu\"></div><div class=\"inner_bar\" id=\"bar_cpu\"></div></div>";
    echo "<div class=\"label\">RAM Usage:</div><div class=\"bar\"><div class=\"percent\" id=\"percent_ram\"></div><div class=\"inner_bar\" id=\"bar_ram\"></div></div>";
    echo "<div class=\"label\">Disk Usage:</div><div class=\"bar\"><div class=\"percent\" id=\"percent_disk\"></div><div class=\"inner_bar\" id=\"bar_disk\"></div></div>";
    echo "</div></div>";
}

if($_GET["param"] == "l"){
exec("/var/www/monitor.paulfruehauf.de/monitor.py -l", $output);
exec("cat /var/www/monitor.paulfruehauf.de/logging.log | grep ".$grepdate, $output);
echo arrayToString($output);
}

if($_GET["param"] == "s"){
exec("/var/www/monitor.paulfruehauf.de/monitor.py", $output);
exec("cat /var/www/monitor.paulfruehauf.de/logging.log | grep ".$grepdate, $output);
echo arrayToString($output);
}

?>

</body>
</html>