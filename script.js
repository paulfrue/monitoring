var ajaxObj = null;
    ajaxObj = new XMLHttpRequest();

function cpurefresh() {
    if (ajaxObj.readyState == 4) {
        var livecpu = ajaxObj.responseText;
    }
    if(livecpu != undefined){
        document.getElementById("bar_cpu").setAttribute("style", "width: "+livecpu*7+"px");
        document.getElementById("percent_cpu").innerHTML = livecpu+"%";
    }
}

function ajaxloop(){
    ajaxObj.open("GET", "livecpu.php");
    ajaxObj.onreadystatechange = cpurefresh;
    ajaxObj.send(null);
    setTimeout(function(){
        ajaxloop();
    }, 3000);
}

function bar_init(){
    var cpu = document.getElementById("cpu").innerHTML;
    var ram = document.getElementById("ram").innerHTML;
    var disk = document.getElementById("disk").innerHTML;
    document.getElementById("bar_cpu").setAttribute("style", "width: "+cpu*7+"px");
    document.getElementById("bar_ram").setAttribute("style", "width: "+ram*7+"px");
    document.getElementById("bar_disk").setAttribute("style", "width: "+disk*7+"px");
    document.getElementById("percent_cpu").innerHTML = cpu+"%";
    document.getElementById("percent_ram").innerHTML = ram+"%";
    document.getElementById("percent_disk").innerHTML = Math.round(disk, 0)+"%";

    ajaxloop();
}