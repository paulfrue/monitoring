import configparser
import smtplib
from email.message import Message
from time import strftime

# class MailAlarm
class MailAlarm:

    # MailAlarm Constructor
    def __init__(self):

        # Get SMTP data out of settings.ini
        self.sender = self.getSettings("SMTP", "sender")
        self.password = self.getSettings("SMTP", "pass")
        self.receiver = self.getSettings("SMTP", "receiver")
        self.smtphost = self.getSettings("SMTP", "smtphost")
        self.port = self.getSettings("SMTP", "port")

        # Create an object of Message() and declare basic attributes
        self.mail = Message()
        self.mail["From"] = self.sender
        self.mail["To"] = self.receiver

    # Function send_mail; needed parameters: string subject, string text;
    def send_mail(self, subject, text):
        # Starts an SMTP connection and login
        self.server = smtplib.SMTP_SSL(self.smtphost, self.port)
        self.server.ehlo()
        self.server.login(self.sender, self.password)

        # Fill the mail with subject and content
        self.mail["Subject"] = subject
        self.mail.set_payload(text)

        # Send the mail and close the connection
        self.server.sendmail(self.sender, self.receiver, self.mail.as_string())
        self.server.quit()

        # Print a notification on the console
        print(strftime("%d.%m.%Y %H:%M:%S") + " Critical Load: Notification E-Mail has been sent to: " + self.receiver + "\n")

    # Function getSettings; needed parameters: string index, string key
    @staticmethod
    def getSettings(index, key):
        # Read the settings.ini file with configparser and return a value
        settings = configparser.ConfigParser()
        settings.read("./settings.ini")
        return settings[index][key]